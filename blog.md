Consider the following simple smart contract.

```
pragma solidity ^0.6.0;

contract SimpleBankStep1 {
  mapping(address => uint256) internal balances;

  receive() external payable {
    balances[msg.sender] = balances[msg.sender] + msg.value;
  }

  function withdraw(uint256 amount) external {
    require(balances[msg.sender] >= amount);
    msg.sender.transfer(amount);
    balances[msg.sender] = balances[msg.sender] - amount;
  }

  function checkBalance() external view returns(uint256) {
    return balances[msg.sender];
  }
}
```

This is a simple bank contract that allows any account to deposit funds, check their
balance, and withdraw deposited funds. It has a critical vulnerability, can you find it?

Unit tests are the first thing most programmers reach to in order to verify their code.

Here are a few unit tests I've written for this contract.

```
it('should allow a user to make a deposit', async function() {
  let balanceBefore = await myBank.checkBalance({from: accounts[0]})
  await myBank.sendTransaction({from: accounts[0], value: 1000})
  let balanceAfter = await myBank.checkBalance({from: accounts[0]})
  assert(balanceAfter > balanceBefore, `Error: balance after deposit
    should be greater than ${balanceBefore} but is ${balanceAfter}`)
})

it('should allow a user to make a withdrawal', async function() {
  let balanceBefore = await myBank.checkBalance({from: accounts[0]})
  await myBank.withdraw(balanceBefore, {from: accounts[0]})
  let balanceAfter = await myBank.checkBalance({from: accounts[0]})
  assert(balanceAfter == 0, `Error: balance after withdrawal
    should be zero but is ${balanceAfter}`)
})

it('must NOT allow a user to withdraw more funds than they have deposited',
async function() {
  let balanceBefore = await myBank.checkBalance({from: accounts[0]})
  try {
    await myBank.withdraw((balanceBefore + 100), {from: accounts[0]})
  } catch (error) {
    assert(error, "Expected an error but did not get one")
  }
  await myBank.withdraw(balanceBefore, {from: accounts[0]})
  let balanceAfter = await myBank.checkBalance({from: accounts[0]})
  assert(balanceAfter == 0, `Error: balance after withdrawal
    should be zero but is ${balanceAfter}`)
})
```
Unit tests are good for making sure code can do what you want it to, but they're
not very useful for making sure it CAN'T do anything unexpected. After all, the tests
can only check for things that you think about and anticipate!

If you want to try out this code for yourself, clone the project from my github (LINK) and follow the instructions
in the README (create this, LINK).

Today we're going to use a different type of tool, a ***static analyzer***.

***Slither*** is a static analysis tool made by Trail of Bits (LINK).

Static analyzers check code for known vulnerabilities without actually running the code,
so they are a quick and valuable tool that should be used in all smart contract projects.

The important thing to remember is that static analyzers ***only detect known vulnerable patterns***.

Complicated contracts and data structures may be beyond their ability. Don't be
lulled into a sense of false security by growing dependent on tools.

Follow this guide to install slither. (LINK)

Using slither is easy. From your main project directory, run `slither .` to perform an
analysis of all your contracts.

Slither runs various detectors looking for known patterns. Let's check these one
by one.

Note that Truffle projects always include a Migrations.sol contract which helps
in deploying your other contracts to the test blockchain. Read more about truffle here(LINK),
we won't pay any mind to this for now.
```
INFO:Detectors:
Different versions of Solidity is used in :
	- Version used: ['>=0.4.21<0.7.0', '^0.6.0']
	- ^0.6.0 (SimpleBankStep1.sol#1)
	- >=0.4.21<0.7.0 (Migrations.sol#2)
Reference: https://github.com/crytic/slither/wiki/Detector-Documentation#different-pragma-directives-are-used
```
This is good to keep in mind (it's unwise to use different Solidity versions between contracts),
but we won't worry about this for now.

```
INFO:Detectors:
Pragma version^0.6.0 (SimpleBankStep1.sol#1) necessitates versions too recent to be trusted. Consider deploying with 0.5.11
Pragma version>=0.4.21<0.7.0 (Migrations.sol#2) allows old versions
Reference: https://github.com/crytic/slither/wiki/Detector-Documentation#incorrect-versions-of-solidity
```
Another good point. In truth, the Solidity language as a whole is still early stage.

It's best to strike a balance between new versions with fixes and old versions with
inferior features.

```
INFO:Detectors:
Variable Migrations.last_completed_migration (Migrations.sol#6) is not in mixedCase
Reference: https://github.com/crytic/slither/wiki/Detector-Documentation#conformance-to-solidity-naming-conventions
```
A complaint about our naming conventions. We can safely ignore this.

```
INFO:Detectors:
Reentrancy in SimpleBankStep1.withdraw(uint256) (SimpleBankStep1.sol#10-14):
	External calls:
	- msg.sender.transfer(balances[msg.sender]) (SimpleBankStep1.sol#12)
	State variables written after the call(s):
	- balances[msg.sender] = 0 (SimpleBankStep1.sol#13)
Reference: https://github.com/crytic/slither/wiki/Detector-Documentation#reentrancy-vulnerabilities-4
```

Uh oh! We've found the big one. Reentrancy is the classic smart contract bug behind
notorious hacks such as the DAO (link), [link some more examples here].

This is exactly the kind of thing that is our job to detect and avoid as smart contract
developers.

To understand how the reentrancy bug works in depth, read more here. (LINK)

The key thing to understand is that when you make a call to an external contract
or address, you transfer the control flow of your code to that external party.

By calling withdraw() numerous times before their balance is updated, the attacker
can drain the entire balance of the smart contract, not just the balance remaining
in their own account.

Let's fix that! To do so, we need to remove any state changes that happen after
an external call.

```
function withdraw(uint256 amount) external {
  require(balances[msg.sender] >= amount);
  balances[msg.sender] = balances[msg.sender] - amount;
  msg.sender.transfer(amount);
}
```
By putting the external call at the end of the function, we prevent any nefarious
changes from being made after the transfer of control.

The `require()` statement ensures that if the transfer doesn't go through, any
changes made in this function will be reverted.

Let's run slither again and see if that worked.
