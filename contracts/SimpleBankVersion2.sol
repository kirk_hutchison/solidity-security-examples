pragma solidity ^0.6.0;

contract SimpleBankVersion2 {
  mapping(address => uint256) internal balances;

  receive() external payable {
    balances[msg.sender] = balances[msg.sender] + msg.value;
  }

  function withdraw(uint256 amount) external {
    require(balances[msg.sender] >= amount);
    balances[msg.sender] = balances[msg.sender] - amount;
    msg.sender.transfer(amount);
  }

  function checkBalance() external view returns(uint256) {
    return balances[msg.sender];
  }
}
