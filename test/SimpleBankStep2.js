const Bank = artifacts.require("SimpleBankStep2.sol")

contract("SimpleBank", accounts => {
  var myBank
  before(async function() {
    myBank = await Bank.deployed()
    await myBank.sendTransaction({from: accounts[1], value: 1000000})
  })

  it('should allow a user to make a deposit', async function() {
    let balanceBefore = await myBank.checkBalance({from: accounts[0]})
    await myBank.sendTransaction({from: accounts[0], value: 1000})
    let balanceAfter = await myBank.checkBalance({from: accounts[0]})
    assert(balanceAfter > balanceBefore, `Error: balance after deposit
      should be greater than ${balanceBefore} but is ${balanceAfter}`)
  })

  it('should allow a user to make a withdrawal', async function() {
    let balanceBefore = await myBank.checkBalance({from: accounts[0]})
    await myBank.withdraw(balanceBefore, {from: accounts[0]})
    let balanceAfter = await myBank.checkBalance({from: accounts[0]})
    assert(balanceAfter == 0, `Error: balance after withdrawal
      should be zero but is ${balanceAfter}`)
  })

  it('must NOT allow a user to withdraw more funds than they have deposited',
  async function() {
    let balanceBefore = await myBank.checkBalance({from: accounts[0]})
    try {
      await myBank.withdraw((balanceBefore + 100), {from: accounts[0]})
    } catch (error) {
      assert(error, "Expected an error but did not get one")
    }
    await myBank.withdraw(balanceBefore, {from: accounts[0]})
    let balanceAfter = await myBank.checkBalance({from: accounts[0]})
    assert(balanceAfter == 0, `Error: balance after withdrawal
      should be zero but is ${balanceAfter}`)
  })
})
