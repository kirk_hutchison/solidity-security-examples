These install steps are for Ubuntu Linux, but installation should work similarly on any Unix system (Mac or Linux).

To set things up, first clone the repository locally with
`git clone https://gitlab.com/kirk_hutchison/solidity-security-examples.git`

Then, cd into the directory and run `npm install`. This will install the dependencies and tools we need to simulate running your contracts on a real blockchain.

The most important of these are *Truffle*, which provides a framework for us to conveniently test our contracts, and *Ganache*, which lets us run a test blockchain locally.

After running `npm install`, you need to activate ganache. Run the command `npm run ganache` to start a ganache instance in the current tab.

Open a new terminal tab and run `npm run compile` to compile your contracts.

`npm run test1` will run tests on the first version of the contract, while `npm run test2` will test the fixed version. 
